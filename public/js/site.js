﻿$(function(){
    $("input[type='datetime']:not(.start-date):not(.end-date)").datetimepicker();


    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
//    DATETIMEPICKER
    $('').datetimepicker();
    $("input[type='datetime'].start-date, input[type='datetime'].end-date").datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    $("input[type='datetime'].start-date").on("dp.change", function (e) {
        $(this).closest(".row").find("input[type='datetime'].end-date").data("DateTimePicker").minDate(e.date);
    });
    $("input[type='datetime'].end-date").on("dp.change", function (e) {
        $(this).closest(".row").find("input[type='datetime'].start-date").data("DateTimePicker").maxDate(e.date);
    });

    $("form.async-form").on("submit", function(e){
        e.preventDefault();
        $this = $(this);
        $.ajax({
            url: $this.attr("action"),
            type: $this.attr("method"),
            data: $(this).serialize(),
            success: function(data)
            {
                $this.replaceWith(data);
            }
        });
    });
});