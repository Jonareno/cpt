@extends('layouts.master')

@section('body')

    <div id="hero-panel-background">
        {{--<img src="" style="width:100%;max-height:500px;"/>--}}
    </div>
    <div id="hero-panel" class="text-center text-white">
        <br /><br />
        <h1>
            We help personal trainers<br />
            deliver smarter results.
        </h1>
        <br /><br />
        {{--<a href="{{ route('user.register')}}" class="btn btn-lg btn-gold">Sign up for our Beta</a>--}}
        <h4>Coming Soon</h4>

    </div>

    <div class="row">
        <div class="col-md-4 text-center">
            <div class="hero-support smart-workout"></div>
            <h4 class="text-capitalize text-primary">Save time with smart workouts</h4>
            <p>State of the art algorithms instantly personalize workouts for each client, which you can customize as needed.</p>
        </div>
        <div class="col-md-4 text-center">
            <div class="hero-support smart-chat"></div>
            <h4 class="text-capitalize text-primary">Communicate with clients</h4>
            <p>Instantly chat with clients when they struggle mid-workout, providing just-in-time experience and<br />better results.</p>
        </div>
        <div class="col-md-4 text-center">
            <div class="hero-support smart-calendar"></div>
            <h4 class="text-capitalize text-primary">Supercharge your schedule</h4>
            <p>Take control of your schedule with smarter calendaring that supports clients better while maximizing your available time.</p>
        </div>
    </div>

@endsection