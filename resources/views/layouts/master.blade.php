<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="CPT">
		<meta name="author" content="Jonathan White">
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title') - CPT</title>

        <link rel="stylesheet" href="{{ URL::asset('lib/bootstrap/dist/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="{{ URL::asset('lib/bootstrap-datepicker-1.5.1/css/bootstrap-datepicker.css') }}" rel="stylesheet" />

        {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />--}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />

        <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css" rel="stylesheet" />
        {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.print.css" rel="stylesheet" />--}}

        <link rel="stylesheet" href="{{ URL::asset('css/site.css') }}" />
        <style type="text/css">
            div#hero-panel-background {background: linear-gradient( rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6) ), url({{ URL::asset('/images/istock_000012966913_large.jpg') }}) center center;}
        </style>
		@yield('styles')
	</head>
	<body>
    <nav>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="{{ route('home.index') }}" class="navbar-brand">
                        <i class="fa fa-trophy text-yellow"></i> CPT4ME
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                       <!--  <li><a href="{{ route('home.index') }}">How It Works</a></li>
                        <li><a href="{{ route('home.about') }}">Success Stories</a></li>
                        <li><a href="{{ route('home.contact') }}">Pricing</a></li> -->
                    </ul>
                    @if (Auth::check())
                        {
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="{{ route('user.account') }}" title="Manage">
                                        {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
                                        <img src="http://www.fillmurray.com/40/40" height="25" width="25" class="img-circle" />
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void();">
                                        <i class="fa fa-bars"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('user.dashboard') }}" title="Dashboard">Dashboard</a></li>
                                        <li><a href="{{ route('user.account') }}" title="Account">Account</a></li>
                                        <li>
                                            <a href="{{ route('user.logout') }}">Log off</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                    @else
                        {{--<ul class="nav navbar-nav navbar-right">--}}
                        {{--<li><a href="{{ route('user.register') }}">Register</a></li>--}}
                        {{--<li><a href="{{ route('user.login') }}">Log in</a></li>--}}
                        {{--</ul> --}}
                    @endif
                </div>
            </div>
        </div>
        @if (Auth::check())
            <div class="nav-sub">
                <div class="container">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li role="presentation" {{ Route::is('user.dashboard') ? 'class=active' : '' }} ><a href="{{ route('user.dashboard') }}">Dashboard</a></li>

                            @if(Auth::user()->type == Config::get('enums.user_types')["PT"] )
                                <li role="presentation" {{ Route::is('user.link.all') ? 'class=active' : '' }}><a href="{{ route('user.link.all') }}">My Clients</a></li>
                                <li role="presentation" {{ Route::is('user.link.search') ? 'class=active' : 'pony' }}><a href="{{ route('user.link.search') }}">Find Clients</a></li>
                            @endif
                            @if(Auth::user()->type == Config::get('enums.user_types')["Client"] && isset($ptUserID) )
                                <li role="presentation"><a href="{{ route('calendar.schedule', ['linkedUserId' => $ptUserID]) }}">Schedule</a></li>
                            @endif

                            {{--<li role="presentation"><a href="#">Schedule</a></li>--}}
                            {{--<li role="presentation"><a href="#">Workouts</a></li>--}}
                            {{--<li role="presentation"><a href="#">News</a></li>--}}
                            {{--<li role="presentation"><a href="#">Inbox</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </nav>    
    <div class="container body-content">
        @hasSection('title')
            <h2>@yield('title')</h2>
        @else

        @endif

        @if (count($errors) > 0 )
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif

        @yield('body')
    </div>
    <footer class="container">
        <hr />
        <p>&copy; 2017 - CPT4ME</p>
    </footer>
    <script src="{{ URL::asset('lib/jquery/dist/jquery.js') }}"></script>
    <script src="{{ URL::asset('lib/bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="{{ URL::asset('lib/bootstrap-datepicker-1.5.1/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js" type="text/javascript"></script>
    <script src="{{ URL::asset('js/site.js') }}" asp-append-version="true"></script>

    @yield('scripts')
	</body>
</html>