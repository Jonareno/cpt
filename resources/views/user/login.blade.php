@extends('layouts.master')

@section('title')
    Login
@endsection

@section('body')

    <div class="row">
        <div class="col-md-8">
            <section>
                <form action="{{ route('user.login') }}" method="post" class="form-horizontal" role="form">
                    <!-- <h4>Use a local account to log in.</h4> -->
                    <hr />
                    <div asp-validation-summary="ValidationSummary.All" class="text-danger"></div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">Email</label>
                        <div class="col-md-10">
                            <input name="email" id="email" class="form-control" type="text" required autofocus />
                            <span asp-validation-for="email" class="text-danger"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-2 control-label">Password</label>
                        <div class="col-md-10">
                            <input name="password" id="password" class="form-control" type="password" required/>
                            <span asp-validation-for="password" class="text-danger"></span>
                        </div>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-offset-2 col-md-10">--}}
                            {{--<div class="checkbox">--}}
                                {{--<input asp-for="RememberMe" type="checkbox" />--}}
                                {{--<label asp-for="RememberMe">Remember Me</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10 text-right">
                            <a href="{{ route('user.register') }}" class="btn btn-link">Register as a new user?</a>
                            <button type="submit" class="btn btn-primary">Log in</button>
                        </div>
                    </div>
                    <div class="text-right">
                        {{--<p>--}}
                            {{--<a asp-action="ForgotPassword">Forgot your password?</a>--}}
                        {{--</p>--}}
                    </div>
                    {{ csrf_field() }}
                </form>
            </section>
        </div>
        {{--<div class="col-md-4">--}}
            {{--<section>--}}
                {{--<h4>Use another service to log in.</h4>--}}
                {{--<hr />--}}
                {{--@{--}}
                {{--var loginProviders = SignInManager.GetExternalAuthenticationSchemes().ToList();--}}
                {{--if (loginProviders.Count == 0)--}}
                {{--{--}}
                {{--<div>--}}
                    {{--<p>--}}
                        {{--There are no external authentication services configured. See <a href="http://go.microsoft.com/fwlink/?LinkID=532715">this article</a>--}}
                        {{--for details on setting up this ASP.NET application to support logging in via external services.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--}--}}
                {{--else--}}
                {{--{--}}
                {{--<form asp-controller="Account" asp-action="ExternalLogin" asp-route-returnurl="@ViewData["ReturnUrl"]" method="post" class="form-horizontal" role="form">--}}
                {{--<div>--}}
                    {{--<p>--}}
                        {{--@foreach (var provider in loginProviders)--}}
                            {{--{--}}
                            {{--<button type="submit" class="btn btn-default" name="provider" value="@provider.AuthenticationScheme" title="Log in using your @provider.DisplayName account">@provider.AuthenticationScheme</button>--}}
                            {{--}--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--</form>--}}
                {{--}--}}
                {{--}--}}
            {{--</section>--}}
        {{--</div>--}}
    </div>

@endsection