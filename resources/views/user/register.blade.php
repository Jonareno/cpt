@extends('layouts.master')

@section('title')
    @if( Auth::check() && Auth::user()->type == Config::get('enums.user_types')["PT"])
        Create client
    @else
        Register
    @endif
@endsection

@section('body')
    <div class="row">
        <div class="col-md-8">
            <form action="{{ Auth::check() && Auth::user()->type == Config::get('enums.user_types')["PT"] ? route('user.CreateClient') : route('user.register') }}" method="post" class="form-horizontal" role="form">
                <h4>Create a new account.</h4>
                <hr />
                <div class="form-group">
                    <label for="name" class="col-md-2 control-label">Full Name</label>
                    <div class="col-md-10">
                        <input name="name" id="name" class="form-control" required autofocus/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-2 control-label">Email</label>
                    <div class="col-md-10">
                        <input name="email" id="email" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-md-2 control-label">Phone</label>
                    <div class="col-md-10">
                        <input name="phone" id="phone" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-2 control-label">Password</label>
                    <div class="col-md-10">
                        <input name="password" id="password" class="form-control" type="password" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation" class="col-md-2 control-label">Confirm Password</label>
                    <div class="col-md-10">
                        <input name="password_confirmation" id="password_confirmation" class="form-control" type="password"  required/>
                    </div>
                </div>
                @if( Auth::check() && Auth::user()->type == Config::get('enums.user_types')["PT"])
                    <input type="hidden" value="{{ Config::get('enums.user_types')["Client"] }}" />
                @else                    
                    <div class="form-group">
                        <label for="type" class="col-md-2 control-label">Register as</label>
                        <div class="col-md-10">
                            <select name="type" id="type" class="form-control">
                                <option value="{{Config::get('enums.user_types')["Client"]}}">Patient</option>
                                <option value="{{ Config::get('enums.user_types')["PT"] }}">Physical Therapist</option>
                            </select>
                            <span asp-validation-for="ConfirmPassword" class="text-danger"></span>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10 text-right">
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
@endsection