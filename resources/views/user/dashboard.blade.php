@extends('layouts.master')


@section('body')
    @if (isset($message))
    <div class="alert alert-info">
        {{ $message }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <h3>Updates</h3>
            <hr />
            @foreach( $linkedUsers as $user)            
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-circle " src="http://www.fillmurray.com/40/40">
                        </a>
                    </div>
                    <div class="media-body">
                        <a href="#chat-list-{{ $user->id }}" class="btn btn-primary pull-right" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">Chat</a>
                        <h4>{{ $user->name }}</h4>
                        <!-- <p>Is requesting assistance with "Mobility Recovery" workout.</p> -->
                        <br />
                        <ul class="media-list chat-list collapse" id="chat-list-{{ $user->id }}" >

                            <!-- <li class="media">
                                <div class="media-left">
                                    <img class="media-object img-circle " src="http://www.fillmurray.com/40/40">
                                </div>
                                <div class="media-body">
                                    <p class="chat-bubble">
                                        Heya Tyler, I'm not sure I'm doing this extension right. Do you have time to show me the right moves?
                                        <br /><small class="text-muted">Bill Murray | 23rd June at 5:00pm</small>
                                    </p>
                                </div>
                            </li>
                            <li class="media chat-right">
                                <div class="media-body">
                                    <p class="chat-bubble">
                                        Sure, I'll shoot you a quick video in a minute.
                                        <br /><small class="text-muted">Tyler Rexa | 23rd June at 5:00pm</small>
                                    </p>
                                </div>
                                <div class="media-right">
                                    <img class="media-object img-circle " src="http://placecage.com/g/40/40">
                                </div>
                            </li>
                            <li class="media chat-right">
                                <div class="media-body">
                                    <p class="chat-bubble">
                                        There you go. Keep your head up and core tight.
                                        <br />
                                        <img src="https://media.licdn.com/mpr/mpr/p/5/005/07c/04b/12e764b.jpg" width="120" class="img-rounded" />
                                        <br /><small class="text-muted">Tyler Rexa | 23rd June at 5:00pm</small>
                                    </p>
                                </div>
                                <div class="media-right">
                                    <img class="media-object img-circle " src="http://placecage.com/g/40/40">
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-left">
                                    <img class="media-object img-circle " src="http://www.fillmurray.com/40/40">
                                </div>
                                <div class="media-body">
                                    <p class="chat-bubble">
                                        You rock. Thx!
                                        <br /><small class="text-muted">Bill Murray | 23rd June at 5:00pm</small>
                                    </p>
                                </div>
                            </li> -->
                        </ul>

                        <div>
                            <form class="chat-form" action="{{ route('chat.send.to', ['id' => $user->id ]) }}" >
                                <div class="input-group">
                                    <input type="text" name="message" class="form-control" placeholder="Enter your message" id="chat-message" autocomplete="off">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit" id="send-chat">SEND</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach           

            <br />
            <br />

            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" data-src="..." alt="Generic placeholder image" src="http://www.placecage.com/c/40/40">
                    </a>
                </div>
                <div class="media-body">
                    <p>
                        <span class="media-heading bold">Top aligned media</span><br />
                        You have <strong>two</strong> new prospects in your inbox.
                    </p>
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" data-src="..." alt="Generic placeholder image" src="http://www.placecage.com/40/40">
                    </a>
                </div>
                <div class="media-body">
                    <p>
                        <span class="media-heading bold">Mary Wilson</span><br />
                        is celebrating her one year anniversary as your client today.
                    </p>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div id="calendar"></div>
            <button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#addTaskPanel" aria-expanded="false" aria-controls="addTaskPanel">Create a Task</button>
            <div id="addTaskPanel" class="collapse">
                @include('CalendarEvents.edit')
            </div>
            <hr />

            <h3 class="text-center">Today</h3>
            <hr />
            @if(isset($todaysEvents) && $todaysEvents->count() > 0)
                @foreach ($todaysEvents as $event)
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object img-circle" data-src="..." alt="Generic placeholder image" src="http://www.placecage.com/40/40">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <p>
                                        <span class="media-heading bold">{{ $event->title }}</span><br />
                                        {{ \Carbon\Carbon::parse($event->date_start)->format('h:i A') }}
                                    </p>
                                    <p>
                                        {{ $event->description or "" }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="alert alert-info" role="alert">
                You have no workouts for today.
            </div>
            @endif
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#calendar').datepicker({
                todayHighlight: true
            });
            $(".chat-form").submit(function (e) {
                var today = new Date();
                var month = new Array();
                month[0] = "January";
                month[1] = "February";
                month[2] = "March";
                month[3] = "April";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "August";
                month[8] = "September";
                month[9] = "October";
                month[10] = "November";
                month[11] = "December";
                var chatInsert = '<li class="media chat-right">\
                            <div class="media-body">\
                                <p class="chat-bubble">' + $("#chat-message").val() + '\
                                    <br /><small class="text-muted">{{ Auth::user()->name }} | ' + today.getDate() + ' ' + month[today.getMonth()] + ' ' + ' at ' + today.toLocaleTimeString() + '</small>\
                                </p>\
                            </div>\
                            <div class="media-right">\
                                <img class="media-object img-circle " src="http://placecage.com/g/40/40">\
                            </div>\
                        </li>';
                
                var _$form = $(this);
                $.post($(this).attr('action'), $(this).serialize() )
                    .done(function(data){
                        $chatWindow = $(_$form).parent().parent().find(".chat-list");
                        $chatWindow.collapse('show');
                        $chatWindow.append(chatInsert).scrollTop($chatWindow[0].scrollHeight);
                    })
                    .fail(function(data){
                        alert(data.statusText);
                        console.log(data);
                    });

                $(this).trigger("reset");
                e.preventDefault();
                return false;
            });
        });
    </script>
@endsection