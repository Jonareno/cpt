@extends('layouts.master')

@php ($readOnly = $user->id != Auth::user()->id)

@section('title')
    @if ( $readOnly )
        {{ $user->name }}'s Account
    @else
        My Account 
    @endif
@endsection

@section('body')
    @if (isset($phoneValidationMessage))
    <div class="alert alert-info">
        {{ $phoneValidationMessage }}
    </div>
    @endif
    <div class="row">
        <!-- left column -->
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="text-center">
                <!-- <img src="http://lorempixel.com/200/200/people/9/" class="avatar img-circle img-thumbnail" alt="avatar"> -->
                <h1>
                    <i class="fa fa-user-circle-o fa-5x"></i>
                </h1>
                <!-- <h6>Upload a different photo...</h6>
                <input type="file" class="text-center center-block well well-sm"> -->
            </div>
        </div>
        <!-- edit form column -->
        <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
            <h3>Personal info</h3>
            <form class="form-horizontal" role="form" action="{{ route('user.account') }}" method="post">
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="name">Name</label>
                    <div class="col-lg-8">
                        <input id="name" name="name" class="form-control" value="{{ $user->name }}" type="text" {{ $readOnly ? 'disabled="disabled"' : null  }} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="email">Email</label>
                    <div class="col-lg-8">
                        <input id="email" name="email" class="form-control" value="{{ $user->email }}" type="text" {{ $readOnly ? 'disabled="disabled"' : null  }} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="phone">Phone</label>
                    <div class="col-lg-8">
                        <input id="phone" name="phone" class="form-control" value="{{ $user->phone }}" type="phone" {{ $readOnly ? 'disabled="disabled"' : null  }} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Account Type</label>
                    <div class="col-lg-8">
                        <span class="form-control">
                            {{ array_search($user->type, Config::get('enums.user_types') ) }}
                        </span>
                    </div>
                </div>


                @if ( !$readOnly )
                    <div class="form-group text-right">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input class="btn btn-default" value="Reset" type="reset">
                            <input class="btn btn-primary" value="Save Changes" type="submit">                            
                        </div>
                    </div>
                    {{ csrf_field() }}
                @endif
            </form>
        </div>
    </div>

@endsection