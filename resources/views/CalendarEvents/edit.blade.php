<form action="{{ route('calendar.event.edit', ['id' => isset($event) ? $event->id : 0  ] ) }}" method="post"  role="form">
    <h4>Add a Task</h4>
    <hr />
    <div asp-validation-summary="ValidationSummary.ModelOnly" class="text-danger"></div>
    <div class="form-group">
        <label for="title" class="control-label">Name</label>
        <input name="title" id="title" class="form-control" type="text" required />
        <span asp-validation-for="Name" class="text-danger" />
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="date_start" class="control-label">State Date</label>
                <input name="date_start" id="date_start" class="form-control start-date" type="datetime" required />
                <span asp-validation-for="date" class="text-danger" />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="date_end" class="control-label">End Date</label>
                <input name="date_end" id="date_end" class="form-control end-date" type="datetime" required />
                <span asp-validation-for="date" class="text-danger" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="description" class="control-label">Description</label>
        <textarea name="description" id="notes" class="form-control" type="text" required></textarea>
        <span asp-validation-for="Notes" class="text-danger" />
    </div>
    @if(Auth::user()->type == Config::get('enums.user_types')["PT"] && isset($linkedUsers))
        <div class="form-group">
            <label for="for_user" class="control-label">Create Task For</label>
            <select name="for_user" id="for_user" class="form-control">
                <option value="{{ Auth::user()->id }}">My self</option>
                @foreach($linkedUsers as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>
    @endif

    <div class="form-group text-right">
        <input type="submit" value="Create" class="btn btn-default" />
    </div>
    {{ csrf_field() }}
</form>