@extends('layouts.master')

@section('title')
    Schedule
@endsection

@section('body')
    <div>
        <h4><a href="#" class="btn btn-success">You</a> and <a href="#" class="btn btn-primary">{{ $client->name or "" }}</a></h4>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <div id="schedule-calendar"></div>
        </div>
        <div class="col-md-4 well">
            @include('CalendarEvents.edit')
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function(){

            function saveMyData(event) {
                jQuery.post(
                    "{{ route('calendar.event.edit.async', ['id' => '']) }}/" + event.id,
                    {
                        title: event.title,
                        date_start: event.start.format('MM/D/Y h:mm A'),
                        date_end:   event.end.format('MM/D/Y h:mm A')
                    }
                );
            }

            $('#schedule-calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,agendaDay'
                },
                views: {
                    listDay: { buttonText: 'day' },
                    listWeek: { buttonText: 'list week' }
                },
//                select: function (start, end, allDay) {
//                    var title = prompt('Event Title:');
//                    if (title) {
//                        calendar.fullCalendar('renderEvent', {
//                            title: title,
//                            start: start,
//                            end: end,
//                        }, true);
//                    }
//
//                    calendar.fullCalendar('unselect');
//                    saveMyData({'title': title, 'start': start, 'end': end});
//                },
                eventDrop: function (event, dayDelta, minuteDelta) {
                    saveMyData(event);
                },
                eventResize: function (event, dayDelta, minuteDelta) {
                    saveMyData(event);
                },
                navLinks: true, // can click day/week names to navigate views
                eventLimit: true, // allow "more" link when too many events
                editable: true,
                selectable: true,
//                selectHelper: true,
                events: [
                    @foreach($myEvents as $event)
                    {
                        title: '{{ $event->title }}',
                        start: '{{ $event->date_start }}',
                        end: '{{ $event->date_end }}',
                        id: '{{ $event->id }}',
                        description: '{{ $event->description }}',
                        color: '#5cb85c'
                    },
                    @endforeach
                    @foreach($linkedUserEvents as $theirEvents)
                    {
                        @if(Auth::user()->type == Config::get('enums.user_types')["Client"])
                        title: 'PT Unavailable',
                        @else
                        title: '{{ $theirEvents->title }}',
                        @endif
                        start: '{{ $theirEvents->date_start }}',
                        end: '{{ $theirEvents->date_end }}',
                        @if(Auth::user()->type == Config::get('enums.user_types')["PT"])
                        id: '{{ $theirEvents->id }}',
                        @endif
                        description: '{{ $theirEvents->description }}',
                        color: '#337ab7'
                        @if(Auth::user()->type == Config::get('enums.user_types')["Client"])
                            ,editable: false
                        @endif
        },
        @endforeach
    ]
});
        });
    </script>
@endsection