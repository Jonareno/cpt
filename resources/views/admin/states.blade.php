@extends('layouts.admin')

@section('body')
	<h1 class="page-header">States</h1>	
               
	<table class="table table-striped">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Name
				</th>
				<th>
					Active On
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
	@foreach($sitestate as $state)
		<tr>
			<td>
				{{ $state->id }}
			</td>
			<td>
				{{ $state->name }}
			</td>
			<td>
				{{ $state->displayActiveOnDateTime() }}
			</td>
			<td>
				<form method="post" action="{{ route('admin.stateDelete', ['id' => $state->id ]) }}" class="form-inline form-confirm">
					<a href="{{ route('admin.stateEdit', ['id' => $state->id ]) }}" class="btn">Edit</a>
					<button class="btn btn-link" type="submit">Delete</button>
					{{ csrf_field() }}
				</form>
			</td>
		</tr>
	@endforeach
	</table>
	<div class="text-right">
		<a href="{{ route('admin.stateCreate') }}" class="btn btn-primary">Create</a>
	</div>
@endsection