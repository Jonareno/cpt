@extends('layouts.master')

@section('body')
	<h2>Register as an Admin</h2>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			@if (count($errors) > 0 )
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ $error }}</p>
				@endforeach
			</div>
			@endif
			<form method="post" action="{{ route('admin.register') }}">
				<div class="form-group">
					<label for="email">User Name</label>
					<input type="email" name="email" class="form-control" autofocus>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="text-right">
					<button type="submit" class="btn btn-primary">Login</button>
					{{ csrf_field() }}
			</form>
		</div>
	</div>
                
@endsection