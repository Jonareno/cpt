@extends('layouts.admin')

@section('body')
	<h2>{{ $state->id < 1 ? "Create" : "Update" }} State {{ $state->name }}</h2>
	<div class="row">
		<div class="col-md-12">
			<form method="post" action="{{ route('admin.stateEdit', ['id' => $state->id ]) }}">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control" value="{{ $state->name }}">
				</div>
				<div class="form-group">
					<label for="activeOnDate">Active On Date</label>
					<input type="datetime" name="activeOnDate" class="form-control" value="{{ $state->activeOnDate }}">
				</div>
				<div class="form-group">
					<label for="isActive">Is Active? 
					<input type="hidden" name="isActive" value="0">
					<input type="checkbox" name="isActive" class="" value="1" {{ $state->isActive ? "checked=checked" : "" }}  >					
				</label>
					
				</div>
				<div class="form-group">
					<label for="createdBy">Created By</label>
					<input type="text" name="createdBy" class="form-control" disabled="disabled" value="{{ $state->createdByUser->email or ""  }}">
				</div>
				<div class="form-group">
					<label for="lastChangedBy">Last Updated By</label>
					<input type="text" name="lastChangedBy" class="form-control" disabled="disabled" value="{{ $state->lastChangedByUser->email or ""  }}">
				</div>
				
				<div class="text-right">
					<button type="submit" class="btn btn-primary">{{ $state->id < 1 ? "Create" : "Update" }}</button>
				</div>
					{{ csrf_field() }}
			</form>
		</div>
	</div>
                
@endsection