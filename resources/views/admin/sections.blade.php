@extends('layouts.admin')

@section('body')
	<h1 class="page-header">Sections</h1>	
               
	<table class="table table-striped">
		<thead>
			<tr>
				<th>
					ID
				</th>
				<th>
					Name
				</th>
				<th>
					Site State
				</th>
				<th>
					Actions
				</th>
			</tr>
		</thead>
	@foreach($sitesections as $sections)
		<tr>
			<td>
				{{ $sections->id }}
			</td>
			<td>
				{{ $sections->name }}
			</td>
			<td>
				{{ $sections->stateDisplayName() }}
			</td>
			<td>
				<form method="post" action="{{ route('admin.sectionDelete', ['id' => $sections->id ]) }}" class="form-inline form-confirm">
					<a href="{{ route('admin.sectionEdit', ['id' => $sections->id ]) }}" class="btn">Edit</a>
					<button class="btn btn-link" type="submit">Delete</button>
					{{ csrf_field() }}
				</form>
			</td>
		</tr>
	@endforeach
	</table>
	<div class="text-right">
		<a href="{{ route('admin.sectionCreate') }}" class="btn btn-primary">Create</a>
	</div>
@endsection
