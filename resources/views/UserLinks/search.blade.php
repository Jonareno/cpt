@extends('layouts.master')

@section('title')
    Search All Clients
@endsection

@section('body')
    <hr />
    <div class="text-right">
        <a href="{{ route('user.edit', ['id' => 0 ]) }}" class="btn btn-link">Create Client</a>
    </div>
    <div class="well">
        <div class="row">
            <div class="col-xs-12">
                <section>
                    <form action="{{ route('user.link.search') }}" method="post" class="form" role="form">
                        <div asp-validation-summary="ValidationSummary.All" class="text-danger"></div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input name="email" id="email" class="form-control" type="text" required autofocus value="{{ $email or "" }}" />
                            <span asp-validation-for="email" class="text-danger"></span>
                        </div>                    
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </section>
            </div>
        </div>
    </div>
    

    @if(isset($searchResult) && $searchResult->count() > 0)
    <br>
    <div class="">
        <table class="table table-striped">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th></th>
            </thead>
            <tbody>
                
            @foreach($searchResult as $user)
            <tr>
                <td>
                    {{ $user->name }}
                </td>
                <td>
                    {{ $user->email }}
                </td>
                <td class="text-right">
                    <ul class="nav nav-pills pull-right" role="tablist">
                        <li role="presentation" class="">
                            <form action="{{ route('user.link', ['id' => $user->id ]) }}" method="POST" class="form-inline async-form">
                                <button type="submit" class="btn btn-primary">Link Client</button>
                                {{ csrf_field() }}
                            </form>                            
                        </li>
                        <li role="presentation" class="">
                            <div class="form-group">
                                <a href="{{ route('user.profile', ['id' => $user->id ]) }}" class="btn btn-default">Profile</a>
                            </div>
                        </li>
                        <li role="presentation" class="">
                            <form action="{{ route('user.unlink', ['id' => $user->id ]) }}" method="POST" class="form-inline async-form">
                                <button type="submit" class="btn btn-danger">Unlink Client</button>
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    @else
    <div class="alert alert-info">
        No results.
    </div>
    @endif

@endsection