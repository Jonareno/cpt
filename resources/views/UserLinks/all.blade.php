@extends('layouts.master')

@section('title')
    Connections
@endsection

@section('body')
    @if(!empty($linkedUsers) && $linkedUsers->count() > 0)
        @foreach($linkedUsers as $user)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-circle" data-src="..." alt="Generic placeholder image" src="http://www.placecage.com/40/40">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <p>
                                        <span class="media-heading bold">{{ $user->name or "" }}</span><br>
                                        <a href="mailto:{{ $user->email or "" }}">{{ $user->email or "" }}</a>
                                    </p>
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="{{ route('user.profile', ['id' => $user->id ]) }}" class="btn btn-default">Profile</a>

                                    <a href="{{ route('calendar.schedule', [ 'linkedUserId' => $user->id] ) }}" class="btn btn-primary">Manage</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @else
    <div class="alert alert-info">
        <p>You don't haave any connections yet. Go get some!</p>
    </div>
    @endif
@endsection