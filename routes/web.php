<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
	'uses' => 'HomeController@get_home',
	'as' => 'home.index'
]);

Route::get('/Contact', [
    'uses' => 'HomeController@get_contact',
    'as' => 'home.contact'
]);

Route::get('/About', [
    'uses' => 'HomeController@get_about',
    'as' => 'home.about'
]);

Route::post('/Chat/Send/{id}', [
    'uses' => 'ChatController@post_send_chat_message',
    'as' => 'chat.send.to'
]);

Route::group(['middleware' => 'guest'], function() {

    Route::get('/Login', [
        'uses' => 'UserController@get_login',
        'as' => 'user.login'
    ]);

    Route::post('/Login', [
        'uses' => 'UserController@post_login',
        'as' => 'user.login'
    ]);

    Route::get('/Register', [
        'uses' => 'UserController@get_register',
        'as' => 'user.register'
    ]);

    Route::post('/Register', [
        'uses' => 'UserController@post_register',
        'as' => 'user.register'
    ]);

});

Route::group(['middleware' => 'auth'], function(){

    Route::get('/Logout', [
        'uses' => 'UserController@get_logout',
        'as' => 'user.logout'
    ]);

    Route::get('/Dashboard', [
        'uses' => "UserController@get_dashboard",
        'as' => 'user.dashboard'
    ]);

    Route::get('/Me', [
        'uses' => "UserController@get_account",
        'as' => 'user.account'
    ]);

    Route::post('/Me', [
        'uses' => "UserController@post_userProfile",
        'as' => 'user.account'
    ]);

    Route::group(['prefix' => 'User'], function(){

        Route::get('/{id}', [
            'uses' => "UserController@get_userProfile",
            'as' => 'user.profile'
        ]);

        Route::get('/Edit/{id}', [
            'uses' => "UserController@get_createClient",
            'as' => 'user.edit'
        ]);

        Route::post('/Edit/CreateClient', [
            'uses' => "UserController@post_createClient",
            'as' => 'user.CreateClient'
        ]);

        Route::group(['prefix' => 'Link'], function(){
            Route::post('/Link/{id}', [
                'uses' => "UserController@post_LinkToUser",
                'as' => 'user.link'
            ]);

            Route::post('/UnLink/{id}', [
                'uses' => "UserLinkController@post_UnlinkToUser",
                'as' => 'user.unlink'
            ]);
        });
    });


    Route::group(['prefix' => 'Events'], function(){
       Route::post('Edit/{id}', [
            'uses' => 'CalendarEventController@post_EditTask',
            'as' => 'calendar.event.edit'
        ]);
        Route::post('/Async/Edit/{id}', [
            'uses' => 'CalendarEventController@post_EditTask_async',
            'as' => 'calendar.event.edit.async'
        ]);
    });

    Route::group(['prefix' => 'Clients'], function(){
        Route::get('/', [
            'uses' => 'UserLinkController@get_all',
            'as' => 'user.link.all'
        ]);

        Route::get('/Search', [
            'uses' => 'UserLinkController@get_SearchUserLinks',
            'as' => 'user.link.search'
        ]);

        Route::post('/Search', [
            'uses' => 'UserLinkController@post_SearchUserLinks',
            'as' => 'user.link.search'
        ]);
    });

    

    Route::get('/Clients/Schedule/{linkedUserId}', [
        'uses' => 'CalendarController@get_schedule',
        'as' => 'calendar.schedule'
    ]);

});

//
//Route::group(['prefix' => 'admin'], function(){
//	Route::group(['middleware' => 'guest'], function(){
//		Route::get('/', [
//			'uses' => 'AdminController@getLogin',
//			'as' => 'admin.login'
//		]);
//
//		Route::post('/', [
//			'uses' => 'AdminController@postLogin',
//			'as' => 'admin.login'
//		]);
//
//		Route::get('/register', [
//			'uses' => 'AdminController@getRegister',
//			'as' => 'admin.register'
//		]);
//
//		Route::post('/register', [
//			'uses' => 'AdminController@postRegister',
//			'as' => 'admin.register'
//		]);
//
//	});
//
//	Route::group(['middleware' => 'auth'], function(){
//		Route::get('/logout', [
//			'uses' => 'AdminController@getLogout',
//			'as' => 'admin.logout'
//		]);
//		Route::get('/dashboard', [
//			'uses' => 'AdminController@getDashboard',
//			'as' => 'admin.dashboard'
//		]);
//
//
//		Route::get('/states', [
//			'uses' => 'SiteStateController@getStates',
//			'as' => 'admin.states'
//		]);
//		Route::get('/states/create', function(){
//		    return redirect()->route('admin.stateEdit', ['id' => 0]);
//        })->name('admin.stateCreate');
//		Route::get('/states/edit/{id}', [
//			'uses' => 'SiteStateController@getEditState',
//			'as' => 'admin.stateEdit'
//		]);
//		Route::post('/states/edit/{id}', [
//			'uses' => 'SiteStateController@postEditState',
//			'as' => 'admin.stateEdit'
//		]);
//		Route::post('/states/delete/{id}', [
//			'uses' => 'SiteStateController@postDeleteStates',
//			'as' => 'admin.stateDelete'
//		]);
//
//
//
//		Route::get('/sections', [
//			'uses' => 'SiteSectionController@getSections',
//			'as' => 'admin.sections'
//		]);
//		Route::get('/sections/create', function(){
//            return redirect()->route('admin.sectionEdit', ['id' => 0]);
//        })->name('admin.sectionCreate');;
//		Route::get('/sections/edit/{id}', [
//			'uses' => 'SiteSectionController@getEditSection',
//			'as' => 'admin.sectionEdit'
//		]);
//		Route::post('/sections/edit/{id}', [
//			'uses' => 'SiteSectionController@postEditSection',
//			'as' => 'admin.sectionEdit'
//		]);
//		Route::post('/sections/delete/{id}', [
//			'uses' => 'SiteSectionController@postDeleteSection',
//			'as' => 'admin.sectionDelete'
//		]);
//	});
//});
