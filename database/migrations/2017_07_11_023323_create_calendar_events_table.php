<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->dateTime('date_start')->nullable();
            $table->boolean('isActive')->default(true);
            $table->boolean('isDeleted')->default(false);

            $table->integer('createdBy')->unsigned();
            $table->foreign('createdBy')->references('id')->on('users');
            $table->integer('lastChangedBy')->unsigned();
            $table->foreign('lastChangedBy')->references('id')->on('users')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
