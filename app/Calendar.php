<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Requests;
use Validator;

class Calendar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ownedBy', 'lastChangedBy', 'lastChangedBy', 'isActive', 'isDeleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'ownedBy', 'isActive', 'isDeleted'
    ];

    protected $guarded = [ 'id'];

    protected $attributes = [
        'id' => '0'
    ];

    public function createdByUser(){
        return $this->belongsTo('App\User', 'createdBy');
    }

    public function lastChangedByUser(){
        return $this->belongsTo('App\User', 'lastChangedBy');
    }

    public function save(array $options = array())
    {
        if( !$this->ownedBy)
        {
            $this->ownedBy = Auth::user()->id;
        }
        if (!$this->lastChangedBy){
            $this->lastChangedBy = Auth::user()->id;
        }
        parent::save($options);
    }
}
