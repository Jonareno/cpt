<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class UserLink extends Model
{
    protected $fillable = ['linkedUserId'];

    protected $guarded = [ 'id', 'userId'];

    public $timestamps = false;

    public function save(array $options = array())
    {
        if( !$this->userId)
        {
            $this->userId = Auth::user()->id;
        }
        parent::save($options);
    }
}
