<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Http\Requests;
use Validator;

class CalendarEvent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'date_start', 'date_end', 'isActive', 'isDeleted', 'createdBy', 'lastChangedBy'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'createdBy', 'lastChangedBy'
    ];

    protected $guarded = [ 'id'];

    protected $attributes = [
        'id' => '0'
    ];

    public function createdByUser(){
        return $this->belongsTo('App\User', 'createdBy');
    }

    public function lastChangedByUser(){
        return $this->belongsTo('App\User', 'lastChangedBy');
    }

    public function save(array $options = array())
    {
        if( !$this->createdBy)
        {
            $this->createdBy = Auth::user()->id;
        }
        if (!$this->lastChangedBy){
            $this->lastChangedBy = Auth::user()->id;
        }
        parent::save($options);
    }
}
