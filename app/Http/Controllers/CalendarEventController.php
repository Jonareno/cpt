<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Calendar;
use App\CalendarEvent;
use Validator;
use DateTime;

class CalendarEventController extends Controller
{
//    public function _checkForLoggedInUser(){
//        if (Auth::check())
//        {
//
//        }
//    }

    public function get_AllTasks(){
//        $cal = Calendar::where('ownedBy', '==', Auth::user()->id);
//        $
    }

    public function get_EditTask(){
        return view('CalendarEvents.edit');
    }

    public function post_EditTask($id, Request $request){
        $this->post_EditTask_async($id,$request, false);

        return redirect()->back();
    }

    public function post_EditTask_async($id, Request $request, $isAsyncCall = true){
        if ($id < 1){
            $event = new CalendarEvent();
        } else {
            $event = CalendarEvent::findOrFail($id);
        }

        $cal = Calendar::where('ownedBy', '=', Auth::user()->id)->first();

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'date_start' => 'required|date',
            'date_end' => 'required|date'
        ]);
        if ($validator->fails()) {
            if ($isAsyncCall){
                return response()->json($validator->messages(), 500);
            } else {
                return view('user.dashboard', ['event' => $event])->withErrors($validator);
            }
        }

        $event->title = $request->input('title');
        $event->date_start = DateTime::createFromFormat("m/d/Y h:i A", $request->input('date_start'))->format("Y-m-d H:i:s");
        $event->date_end = DateTime::createFromFormat("m/d/Y h:i A", $request->input('date_end'))->format("Y-m-d H:i:s");
        $event->description = $request->input('description');
        $event->linkedCalendarId = $cal->id;
        if ($request->has('for_user'))
            $event->createdBy = $request->input('for_user');

        $event->save();
    }

    public function post_DeleteTask(){}


}
