<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserLink;
use Config;

class UserLinkController extends Controller
{
    public function get_SearchUserLinks(){
    	return view('UserLinks.search');
    }

    public function post_SearchUserLinks(Request $request){
		$this->validate($request, [
            'email' => 'email|required'
        ]);

        $users = User::where('type', Config::get('enums.user_types')["Client"])
            ->where('email', 'LIKE', '%'.$request->input('email').'%')->get();

    	return view('UserLinks.search', [ 'searchResult' => $users, 'email' => $request->input('email') ]);
    }

    public function get_all(){
        $linkedUserIds = UserLink::where('userId', '=', Auth::user()->id)->pluck('linkedUserId');

        $users = User::whereIn('id', $linkedUserIds)->get();

        return view("UserLinks.all", ['linkedUsers' => $users]);
    }

    public function post_LinkToUser($id, Request $request){
        $priorLinkCount = UserLink::where('linkedUserId', $id)
            ->where('userId', Auth::user()->id)
            ->count();

        if ($priorLinkCount == 0){
            $link = new UserLink([
                    'linkedUserId' => $id
                ]);
            $link->save();
            return "<button type='button' disabled='disabled' class='btn btn-link'>Linked</button>";
        }
        else {
            return response()->json("User already linked", 500);
        }

    }

    public function post_UnlinkToUser($id, Request $request){
        $link = UserLink::where('linkedUserId', $id)
            ->where('userId', Auth::user()->id)
            ->firstOrFail();
        
        $link->delete();
        
        return "<button type='button' disabled='disabled' class='btn btn-link'>Unlinked</button>";
    }
}
