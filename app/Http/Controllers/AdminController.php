<?php
	
	namespace App\Http\Controllers;
	
	use App\User;
	use App\SiteState;
	use App\SiteSection;
	use Illuminate\Http\Request;
	use Auth;	
	use App\Http\Requests;
	
	class AdminController extends Controller 
	{
		
		public function getRegister(){
			return view('admin.register');
		}
		
		public function postRegister(Request $request){
			
			$this->validate($request, [
				'email' => 'email|required|unique:users',
				'password' => 'required|min:4'
			]);
			
			$user = new User([
				'email' => $request->input('email'),
				'password' => bcrypt($request->input('password'))
			]);
			
			$user->save();
			
			Auth::login($user);
			
			return redirect()->route('home.index');
		}
		
		
		public function getLogin(){
			return view('admin.login');
		}
		
		public function postLogin(Request $request){
			$this->validate($request, [
				'email' => 'email|required',
				'password' => 'required|min:4'
			]);
			
			if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password') ])){
				return redirect()->route('admin.dashboard');
			}						
			
			return redirect()->back();
		}
		
		public function getLogout(){
			Auth::logout();
			return redirect()->route('home.index');
		}
		
		public function getDashboard(){
			return View('admin.dashboard');
		}
		
	}