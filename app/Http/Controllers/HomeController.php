<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function get_home(){
	    return view('home.index');
    }

    public function get_contact(){
        return view('home.contact');
    }

    public function get_about(){
        return view('home.about');
    }
}
