<?php

namespace App\Http\Controllers;

use App\CalendarEvent;
use App\UserLink;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Calendar;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
// TODO Move to a chat service and delete this garbage!
use Twilio\Rest\Client;

class UserController extends Controller
{
    function get_register(){
        return view('user.register');
    }

    function post_register(Request $request){
        $this->validate($request, [
            'name' => 'required|min:2',
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);

        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),            
            'password' => bcrypt($request->input('password')),
            'type' => $request->input('type')
        ]);

        $user->save();

        Auth::login($user);

        $cal = new Calendar([
            'name' => 'My Calendar'
        ]);

        $cal->save();

        //verify phone number
        $validationTokenMessage = "Please verify your phone number with the follow code: ";        
        if ($request->input('phone') != null){
            $sid = env('PHONE_SID');
            $token = env('PHONE_TOKEN');
            $client = new Client($sid, $token);
            
            $validationRequest = $client->validationRequests->create(
                "+1".$request->input('phone'),
                array(
                    "friendlyName" => $request->input('name')
                )
            );

            $validationTokenMessage = $validationTokenMessage.$validationRequest->validationCode;
        }

        return redirect()->route('user.dashboard', ['message' => $validationTokenMessage ]);
    }

    function get_createClient(){
        return $this->get_register();
    }

    function post_createClient(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);

        $user = new User([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'type' => Config::get('enums.user_types')["Client"]
        ]);

        $user->save();

        //now link user to PT
        //TODO move this to a central inherited class
        app('App\Http\Controllers\UserLinkController')->post_LinkToUser($user->id, $request);

        return redirect()->route('user.profile', ['id' => $user->id]);
    }

    function get_login(){
        return view('user.login');
    }

    function post_login(Request $request){
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password') ])){
            return redirect()->route('user.dashboard');
        } else {
            return redirect()->route('user.login')->withErrors(['Username or password is incorrect.']);
        }

        return redirect()->back();
    }

    function get_logout(){
        Auth::logout();
        return redirect()->route('home.index');
    }

    function get_dashboard(){
        $todaysEvents = CalendarEvent::whereDate('date_start', '>', Carbon::now()->subDays(1))
            ->whereDate('date_start', '<', Carbon::now()->subDays(0))
            ->where('createdBy', '=', Auth::user()->id)
            ->get();

        $ptUserID = null;
        if(Auth::user()->type == Config::get('enums.user_types')["Client"])
            $ptUserID = UserLink::where('userId', '=', Auth::user()->id)->pluck('linkedUserId')->first();

        //get linked users
        $linkedUserIds = UserLink::where('userId', '=', Auth::user()->id)->pluck('linkedUserId');
        $linkedUsers = User::whereIn('id', $linkedUserIds)->get();

        $message = $_GET['message'];

        return view('user.dashboard', ['todaysEvents'=> $todaysEvents, 'ptUserID' => $ptUserID, 'linkedUsers' => $linkedUsers, 'message' => $message ]);
    }

    function get_account(Request $request){
        return $this->get_userProfile(Auth::user()->id, $request);
    }

    function get_userProfile($id, Request $request){
        $user = User::find($id);
        return view('user.account', ['user' => $user]);
    }

    function post_userProfile(Request $request){

        $user = Auth::user();

        
        // $this->validate($request, [
            
        // ]);

        if ($user->email == $request->input('email')){
            $validator = Validator::make(
                $request->all(),
                array(
                    'email' => 'email|required',
                    'name' => 'required|min:2'
                )
            );
        } else {
            $validator = Validator::make(
                $request->all(),
                array(
                    'email' => 'email|required|unique:users',
                    'name' => 'required|min:2'
                )
            );
        }

        $validator->validate();

        $validationTokenMessage = "Please verify your phone number with the follow code: ";
        if ($user->phone != $request->input('phone')){
            $sid = env('PHONE_SID');
            $token = env('PHONE_TOKEN');
            $client = new Client($sid, $token);
            
            $validationRequest = $client->validationRequests->create(
                "+1".$request->input('phone'),
                array(
                    "friendlyName" => $request->input('name')
                )
            );

            $validationTokenMessage = $validationTokenMessage.$validationRequest->validationCode;
        }

        // if ($validator->fails()) {
        //     return view('user.account', ['user' => $user])
        //                 ->withErrors($validator);
        // }

        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->phone = $request->input('phone');
        $user->save();

        return view('user.account', ['user' => $user, 'phoneValidationMessage' => $validationTokenMessage ]);
    }


}
