<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\CalendarEvent;
use App\User;
use App\UserLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Config;
use DB;

class CalendarController extends Controller
{
    public function get_schedule($linkedUserId){

        if (!UserLink::where('userId', '=', Auth::user()->id)->pluck('linkedUserId')->contains($linkedUserId) ){
            return redirect()->back();
        }

        $linkedUserEvents = CalendarEvent::where('createdBy', '=', $linkedUserId)->get();

        $myEvents = CalendarEvent::where('createdBy', '=', Auth::user()->id)->get();

        $client = User::where('id', '=', $linkedUserId)->first();

        $linkedUsers = null;
        if(Auth::user()->type == Config::get('enums.user_types')["PT"])
            $linkedUsers = User::whereIn('id',
                UserLink::where('userId', '=', Auth::user()->id)
                    ->pluck('linkedUserId') )
                ->get();


        return view('calendar.schedule', ['myEvents' => $myEvents, 'linkedUserEvents' => $linkedUserEvents, 'client' => $client, 'linkedUsers' => $linkedUsers ]);
    }
}
