<?php

namespace App\Http\Controllers;
use App\User;

// Required if your environment does not handle autoloading
//require __DIR__ . '/vendor/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    private function add_caller_id($phone, $name){
        $sid = env('PHONE_SID');
        $token = env('PHONE_TOKEN');
        $client = new Client($sid, $token);
        $validationTokens = array();

        $outgoingCallerIds = $client->outgoingCallerIds->read(
            array("phoneNumber" => "+1".$phone),
            array("friendlyName" => $name)
        );

        foreach ($outgoingCallerIds as $outgoingCallerId) {
            array_push($validationTokens, $outgoingCallerId->ValidationCode);
        }

        return $validationTokens;
    }

    private function send_message ($toPhoneNumber, $message){
        // Your Account SID and Auth Token from twilio.com/console
        $sid = env('PHONE_SID');
        $token = env('PHONE_TOKEN');
        $client = new Client($sid, $token);

        //Use the client to do fun stuff like send text messages!
        $client->messages->create(
            // the number you'd like to send the message to
            //'+17816368424',
            '+1'.$toPhoneNumber,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => env('PHONE_NUM'),
                // the body of the text message you'd like to send
                'body' => $message
            )
        );

        
        return '';
    }

    public function post_send_chat_message ($userId, Request $request ){
        $user = User::find($userId);

        if ( $request->input('message') == null )
            abort(500, 'message connot be empty.');
            
        if ( $user->phone == null)
            abort(500, 'user hasn\'t set phone number yet');

        $this->send_message($user->phone, $request->input('message') );
        return "sent";
    }
}
